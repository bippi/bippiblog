import React from 'react'
import styles from './Header.less'

const Header = React.createClass({
	render: function(){
		return (
		 	<header className={styles.header}>
		 		<h1>Bippi blog </h1>
		 	</header>
		)
	}
})


export default Header