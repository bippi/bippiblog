import React from 'react'
import {NavLink} from 'react-router-dom'
import styles from './PostListItem.less'

let getImage = function(imgObj){
	console.log(imgObj);
	if(imgObj){
		return <img src={`${imgObj.fields.file.url}?fit=scale&w=100&h=100`} />
	}

}

const PostListItem = (props) => {
	return (
		<div className={styles.listItem}>
			<div className={styles.image}>
				{getImage(props.fields.mynd)}
			</div>
			<div className={styles.content}>
	 			<h1><NavLink to={`blog/${props.fields.slug}`}>{props.fields.titill}</NavLink></h1>
	 			<div className={styles.entry}>{props.fields.entry}</div>
	 		</div>
	 	</div>
	)
}


export default PostListItem