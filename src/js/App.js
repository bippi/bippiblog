import React from 'react';
import {render} from 'react-dom';
import Layout from './Containers/Layout/Layout';
import global from './Global.less'
//import 'normalize.css';


render(<Layout />, document.getElementById('app'));
