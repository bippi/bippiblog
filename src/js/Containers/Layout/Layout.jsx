import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  Switch
} from 'react-router-dom'
import Header from '../../Components/Header/Header'
import PostList from '../PostList'
import Post from '../Post/Post'
import styles from './layout.less'
import NothingFound from '../../Components/404'



const Layout = React.createClass({
	getInitialState: function(){
		return {
			
		}
	},
	render: function(){
		return(
			<Router forceRefresh={false} basename='/'>
				<div className={styles.wrap}>
					<Header currFlokkur={this.state.currFlokkur} navOpen={this.state.sideNavOpen} toggleSideNav={this.toggleSideNavState}/>
					<div className={styles.content}>
					<Switch>
						<Redirect exact from="/" to="/blog"/>
						<Route exact path="/blog" render={routeProps => <PostList {...routeProps} /> }/>
						<Route path="/blog/:id" render={routeProps => <Post {...routeProps} /> } />
						<Route component={NothingFound} />
					</Switch>
					</div>
				</div>
			</Router>
		)
	}
})



export default Layout