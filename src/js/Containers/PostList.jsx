import React from 'react'
import Post from '../Components/PostListItem/PostListItem'
import {createClient}  from 'contentful'

const PostList = React.createClass({
	getInitialState: function(){
		return {
			posts: []
		}
	},

	componentWillMount: function(){
		this.client = createClient({
		  space: 'ci7km86dufs4',
		  accessToken: 'f7a6288b2fee532a3d255bc9e5db96f893a5022bed2791f8028970f502b501ff'
		})
	},

	componentDidMount: function(){
		this.getPosts();
		
	},

	getPosts: function(){
		this.client.getEntries({content_type: 'blog'}).then((response) => {
			console.log(response);
			this.setState({posts: response.items});
		})
	},

	render: function(){
		return(
			<div>
			{this.state.posts.map((post, key) => {
				console.log(post);
				return(<Post {...post} key={`post-${post.sys.id}`}/>)
			})}
			</div>
		)
	}
})


export default PostList