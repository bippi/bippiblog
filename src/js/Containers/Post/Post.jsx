import React, {Component} from 'react'
import {createClient}  from 'contentful'
import marked from 'marked'
import styles from './Post.less'
import moment from 'moment'

class Post extends Component{
	constructor (){
		super()
		this.state = {post: null, isLoading: true}
	}

	componentWillMount (){
		this.client = createClient({
		  space: 'ci7km86dufs4',
		  accessToken: 'f7a6288b2fee532a3d255bc9e5db96f893a5022bed2791f8028970f502b501ff'
		})
	}

	componentDidMount (){
		moment.locale('is');
		this.getPosts();
	}

	getImage (imgObj){
		console.log('imageObj',  imgObj);
		if(imgObj){
			return <img src={`${imgObj.fields.file.url}?w=800`} />
		}
	}


	getParsedMarkdown (content){
		console.log('content is:', content);
		if(!content){
			return
		}
		return {__html: marked(content, {sanitize: true})};
	}

	getPosts (){
		this.client.getEntries({content_type: 'blog', 'fields.slug': this.props.match.params.id}).then((response) => {
			console.log(response);
			this.setState({
				post: response.items[0],
				isLoading: false
			});
		})
	}

	render (){
		console.log(this.state.post);
		if(this.state.isLoading){
			return <div>Sæki gögn</div>
		}

		if(!this.state.post && !this.state.isLoading){
			console.log('state is null');
			return <h1>Not found</h1>
		}

		return  (
			<div>
				 
					<h1>{this.state.post.fields.titill}</h1>
					<time className={styles.date} >{moment(this.state.post.sys.createdAt).format('DD. MMMM YYYY - HH:mm')}</time>
					<div className={styles.image}>
						{this.getImage(this.state.post.fields.mynd)}
					</div>
					<div dangerouslySetInnerHTML={this.getParsedMarkdown(this.state.post.fields.mainText)} />
				
			</div>
		)
	}
}


export default Post