const path = require('path');
var webpack = require("webpack");
var ExtractTextPlugin = require('extract-text-webpack-plugin');
require('postcss-loader');

module.exports = {
	context: __dirname,
	entry: {
		bundle: ['babel-polyfill', './src/js/App'],
		init: './src/js/init'
	},
	devtool: 'cheap-module-source-map',
	devtool: 'eval',
	output: {
		path: path.join(__dirname, '/public/js'),
		filename: '[name].js'
	},
	resolve: {
		modulesDirectories: ['node_modules', 'src/js'],
		extensions: ['', '.js', '.jsx']

	},
	stats: {
		colors: true,
		reasons: true,
		chunks: true,
		errorDetails: true
	},
	module: {
		loaders: [
			{
				exclude: /node_modules/,
				test: [/\.js$/, /\.jsx$/],
				loader: 'babel-loader',
				query: {
					presets: ['es2015', 'react']
				}
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
			  test: /\.less/,
			  loader: ExtractTextPlugin.extract("css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!less-loader!postcss-loader")
			}

		]
	},
	  plugins: [
	  	new ExtractTextPlugin("../stylesheets/main.css"),
	  	// new webpack.optimize.UglifyJsPlugin({
	  	//      include: /\.min\.js$/,
	  	//      minimize: true
	  	//    }),
	  	// new webpack.DefinePlugin({
	  	//     'process.env': {
	  	//       'NODE_ENV': JSON.stringify('production')
	  	//     }
	  	//   })
	  ]
}